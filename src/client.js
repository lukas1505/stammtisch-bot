const tokenJson = require('../token.json');
const discord = require('discord.js');

const client = new discord.Client();
client.login(tokenJson.token);

module.exports = client;
