const play = require('./play');
const pause = require('./pause');
const resume = require('./resume');
const stop = require('./stop');

const commands = [
  {
    key: 'play',
    args: 1,
    blueprint: 'play <URL zu YouTube Video oder Playlist',
    function: play,
  },
  {
    key: 'pause',
    args: 0,
    blueprint: 'pause',
    function: pause,
  },
  {
    key: 'resume',
    args: 0,
    blueprint: 'resume',
    function: resume,
  },
  {
    key: 'stop',
    args: 0,
    blueprint: 'stop',
    function: stop,
  },
];

module.exports = function (message, args) {
  console.log('Got music command: ', args[0]);

  const command = commands.find((com) => {
    return args[0] === com.key;
  });

  console.log(
    'Found music command called: ',
    command ? command.key : 'none',
    ' with arguments: ',
    args.slice(1)
  );

  if (command) {
    if (args.length - 1 === command.args) {
      command.function(message, args.slice(1));
    } else {
      message.reply(
        `Ich konnte dich leider nicht verstehen. Der Befehl ${command.key}
          sollte so aussehen: music ${command.blueprint}`
      );
    }
  } else {
    message.reply('Tut mir leid, ich weiß nicht was du meinst.');
  }
};
