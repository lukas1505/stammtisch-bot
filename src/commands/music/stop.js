const musicData = require('./music-data');

module.exports = function (message, args) {
  if (musicData.isPlaying) {
    console.log('Stop playing music');

    musicData.isPlaying = false;
    musicData.voiceChannel.leave();
    musicData.queue.slice(0, 0);

    return message.channel.send(
      'Die Musik wurde gestoppt und die Abspielliste gelöscht'
    );
  }

  console.log('Tried stopping music - No music is playing');

  return message.reply(
    'Ich spiele gerade keine Musik. Versuche "iris play <URL zu YouTube Viedo oder Playlist>" um Musik abzuspielen'
  );
};
