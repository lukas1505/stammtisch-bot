module.exports = {
  queue: [],
  isPlaying: false,
  volume: 1,
  songDispatcher: null,
  voiceChannel: null,
};
