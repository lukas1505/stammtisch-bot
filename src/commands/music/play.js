const { MessageEmbed } = require('discord.js');
const Youtube = require('simple-youtube-api');
const ytdl = require('ytdl-core');
const youtubeAPI = 'AIzaSyCusd2Orwtasij-04slypdHHHt_NUY7yFw';
const youtube = new Youtube(youtubeAPI);

const musicData = require('./music-data');

async function playSong(queue, message) {
  let voiceChannel;

  try {
    const connection = await queue[0].voiceChannel.join(); // join the user's voice channel

    const stream = ytdl(queue[0].url, {
      quality: 140,
      filter: 'audioonly',
      opusEncoded: true,
      highWaterMark: 1 << 25,
    });

    const dispatcher = connection
      .play(stream)
      .on('start', () => {
        // the following line is essential to other commands like skip
        musicData.songDispatcher = dispatcher;
        dispatcher.setVolume(musicData.volume);
        musicData.voiceChannel = queue[0].voiceChannel;
        // display the current playing song as a nice little embed
        const videoEmbed = new MessageEmbed()
          .setThumbnail(queue[0].thumbnail) // song thumbnail
          .setColor('#e9f931')
          .addField('Spielt gerade:', queue[0].title)
          .addField('Dauer:', queue[0].duration);
        // also display next song title, if there is one in queue
        if (queue[1]) videoEmbed.addField('Nächstes Lied:', queue[1].title);
        console.log('Play Song: ', videoEmbed);
        message.channel.send(videoEmbed); // send the embed to chat
        return queue.shift(); //  dequeue the song
      })
      .on('finish', () => {
        // this event fires when the song has ended
        if (queue.length >= 1) {
          // if there are more songs in queue
          return playSong(queue, message); // continue playing
        } else {
          // else if there are no more songs in queue
          musicData.isPlaying = false;
          return musicData.voiceChannel.leave(); // leave the voice channel
        }
      })
      .on('error', (e) => {
        message.channel.send('Lied konnte nicht abgespielt werden');
        musicData.queue.length = 0;
        musicData.isPlaying = false;
        console.error(e);
        return musicData.voiceChannel.leave();
      });
  } catch (e) {
    console.error(e);
    return voiceChannel.leave();
  }
}

function formatDuration(durationObj) {
  const duration = `${durationObj.hours ? durationObj.hours + ':' : ''}${
    durationObj.minutes ? durationObj.minutes : '00'
  }:${
    durationObj.seconds < 10
      ? '0' + durationObj.seconds
      : durationObj.seconds
      ? durationObj.seconds
      : '00'
  }`;
  return duration;
}

module.exports = async function play(message, args) {
  if (!message.member)
    return message.reply(
      'Du kannst Musik nur von einem Textkanal des Servers aus starten'
    );

  const voiceChannel = message.member.voice.channel;

  if (!voiceChannel)
    return message.reply(
      'Du musst einem Sprachchannel beitreten um Musik abzuspielen'
    );

  let query = args[0];

  if (
    query.match(/^(?!.*\?.*\bv=)https:\/\/www\.youtube\.com\/.*\?.*\blist=.*$/)
  ) {
    try {
      const playlist = await youtube.getPlaylist(query);
      const videosObj = await playlist.getVideos();

      for (let i = 0; i < videosObj.length; i++) {
        const video = videosObj[i].fetch();

        const url = `https://www.youtube.com/watch?v=${video.raw.id}`;
        const title = video.raw.snippet.title;
        const thumbnail = video.thumbnails.high.url;

        let duration = formatDuration(video.duration);

        if (duration == '00:00') duration = 'Live Stream';

        const song = {
          url,
          title,
          duration,
          thumbnail,
          voiceChannel,
        };

        //musicData.queue.push(song);
        musicData.push(song);
      }
      if (musicData.isPlaying == false) {
        console.log('Playlist will be played');
        musicData.isPlaying = true;
        return playSong(musicData.queue, message); // play the playlist
      } else if (musicData.isPlaying == true) {
        console.log('Playlist added to queue');
        return message.channel.send(
          `Playlist - :musical_note:  ${playlist.title} :musical_note:
          wurde zur Abspielliste hinzugefügt`
        );
      }
    } catch (err) {
      console.error(err);
      return message.channel.send('Ich konnte die Playlist nicht finden');
    }
  }

  if (query.match(/^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/)) {
    const url = query;
    try {
      query = query
        .replace(/(>|<)/gi, '')
        .split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);

      const id = query[2].split(/[^0-9a-z_-]/i)[0];
      const video = await youtube.getVideoByID(id);
      const title = video.title;
      const thumbnail = video.thumbnails.high.url;

      let duration = formatDuration(video.duration);

      if (duration == '00:00') duration = 'Live Stream';

      const song = {
        url,
        title,
        duration,
        thumbnail,
        voiceChannel,
      };

      musicData.queue.push(song);

      if (
        musicData.isPlaying == false ||
        typeof musicData.isPlaying == 'undefined'
      ) {
        console.log('Song will be played');
        musicData.isPlaying = true;
        return playSong(musicData.queue, message);
      } else if (musicData.isPlaying == true) {
        console.log('Song added to queue');
        return message.channel.send(
          `${song.title} wurde zur Abspielliste hinzugefügt`
        );
      }
    } catch (err) {
      console.error(err);
      return message.channel.send('Ich konnte das Lied nicht finden');
    }
  }

  console.error('No valid URL');
  return message.reply(
    'Bitte übergebe den Link zu einem YouTube Video oder einer Playlist'
  );
};
