const client = require('../client');
const pong = require('./pong');
const music = require('./music');

const trigger = 'iris';
const commands = [
  { key: 'ping', args: 0, blueprint: 'ping', function: pong },
  {
    key: 'music',
    args: null,
    blueprint: 'music <play, pause, resume, stop, queue> <argumente>',
    function: music,
  },
];

client.on('message', (message) => {
  const text = message.content;
  const args = text.split(' ');

  console.log('Got Message: ', text);

  if (args[0] === trigger) {
    const command = commands.find((com) => {
      return args[1] === com.key;
    });

    console.log(
      'Found command called: ',
      command ? command.key : 'none',
      ' with arguments: ',
      args.slice(2)
    );

    if (command) {
      if (args.length - 2 === command.args || command.args === null) {
        command.function(message, args.slice(2));
      } else {
        message.reply(
          `Ich konnte dich leider nicht verstehen. Der Befehl ${command.key}
          sollte so aussehen: ${command.blueprint}`
        );
      }
    } else {
      message.reply('Tut mir leid, ich weiß nicht was du meinst.');
    }
  }
});
