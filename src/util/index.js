const client = require('../client');

function getName(state) {
  return state.nickname ? state.nickname : state.displayName;
}

function sendMessage(message, channelName) {
  console.log('Send Message: "', message, '" to ', channelName);

  const channel = client.channels.cache.find(
    (channel) => channel.name === channelName && channel.type === 'text'
  );

  channel.send(message);
}

module.exports = {
  sendMessage,
  getName,
};
