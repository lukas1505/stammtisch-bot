module.exports = {
  channels: {
    group: [
      { text: 'stammtisch', speach: 'stammtisch' },
      { text: 'schach', speach: 'schach' },
      { text: 'brot-und-spiele', speach: 'brot-und-spiele' },
      { text: 'minecraft', speach: 'minecraft' },
      { text: 'ece-online', speach: 'ece-online' },
      { text: 'lern-info', speach: 'lern-info' },
      { text: 'tech-talk', speach: 'tech-talk' },
      { text: 'overwatch', speach: 'overwatch' },
      { text: 'tum-online', speach: 'tum-online' },
    ],
    fallback: 'welcome',
  },

  messages: {
    stammtisch: ['ist Stammtisch beigetreten'],
    schach: ['spielt Schach'],
    minecraft: ['spielt Minecraft'],
    'brot-und-spiele': ['ist Brot und Spiele beigetreten'],
    'ece-online': ['lernt'],
    'lern-info': ['lernt'],
    'tech-talk': ['redet über Technik'],
    'tum-online': ['lernt'],
    overwatch: ['spielt Overwatch'],
  },
};
