const client = require('../../client');
const variables = require('../voice-channel');
const util = require('../../util');

client.on('voiceStateUpdate', (oldState, newState) => {
  console.log(
    'Reached Channel: ',
    newState.channel ? newState.channel.name : '---',
    'from',
    oldState.channel ? oldState.channel.name : '---'
  );

  if (oldState.channelID != newState.channelID && newState.channelID) {
    const server = client.guilds.resolve(newState.guild);
    const voice_channel = server.channels.resolve(newState.channel);

    const channel_group = variables.channels.group.find(
      (group) => group.speach === voice_channel.name
    );

    if (channel_group) {
      const message_end =
        voice_channel.members.size > 1 &&
        variables.messages[channel_group.speach].length >= 2
          ? variables.messages[channel_group.speach][1]
          : variables.messages[channel_group.speach][0]; // messages[voice_channel.name]

      util.sendMessage(
        util.getName(newState.member) + ' ' + message_end,
        channel_group.text
      );
    } else {
      util.sendMessage(
        util.getName(newState.member) +
          ' ist ' +
          voice_channel.name +
          ' beigetreten',
        variables.channels.fallback
      );
    }
  }
});
