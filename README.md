# README

## What is this repository for?

This is a private bot for the discord server "Stammtischtalk"

v1.0.0

## How do I get set up?

To set the project up clone it into a local folder and run `npm i` to install all dependencies. Make shure you have [node.js](https://nodejs.org/en/) installed before.

For testing you have to set up your own Discord bot application like [this](https://discordjs.guide/preparations/setting-up-a-bot-application.html#creating-your-bot). Create a file `token.json` in the root of the project and add folowing code:

```json
{
  "token": "<your-acces-token>"
}
```

now you can test the bot over your own application.

To run the Project in development execute: `npm run dev`.

## Contribution guidelines

If you want to add contribute to this project open a new branch with for the feature you would like to add. When you complete the feature make sure you tested it properly. Then create a pullrequest on master. The code will be reviewed and added to the next release.
