FROM jrottenberg/ffmpeg:4.1-alpine
FROM node:latest

# Create the directory
WORKDIR usr/src/bot

COPY package.json .

RUN npm install --verbose

COPY . .

# Start me!
CMD ["npm", "run", "start"]

